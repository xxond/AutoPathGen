# Procedural generating of closed curve

## Generated cave
![](./readme_img/cave.png)
## Path inside the cave
![](./readme_img/path.png)
## Final Visualisation
![](./readme_img/final.png)
## 3d Visialization of Path
![](./readme_img/3d.png)
